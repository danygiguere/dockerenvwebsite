To install the project, run sh local-install.sh

From project root, run: 

docker build -t app:latest -f ./docker/php/Dockerfile .
docker build -t webserver:latest -f ./docker/nginx/Dockerfile .

docker run -it -p 9000:9000 app:latest
docker run -it -p 8080:80 webserver:latest



or with network:



docker network create dockerenv_network

docker run -it -p 9000:9000 --network=dockerenv_network app:latest
docker run -it -p 8080:80 --network=dockerenv_network webserver:latest

docker network inspect dockerenv_network
